# Java Web第五组——[基于Java Web技术的网上商城](https://gitee.com/lovetoeatmeat/java-web-group-5.git)

#### 项目概述

项目的背景：随着经济文化水平的显著提高，人们对生活质量及工作环境的要求也越来越高。购物作为人类的精神食粮，在现代社会中越来越受到重视，大量的产品出现在市场上，人们有了各种各样不同的选择购物方式，而互联网又在逐步深入与应用，这是电子商务网站也越来越多的出现在我们身边，越来越多的人们选择网上交易，网上交易不但给他们带来了便利还节省了路费。这时就要开发一个购物系统，基于SSH的在线购物平台系统就是其中的一种。

实现功能：商城管理员进入系统后可对用户信息、商品类别、商品信息、留言信息、新闻信息、订单信息等进行管理；普通用户注册成功并登陆系统后，可以浏览商品、加入购物车、下单、查看和修改自己的信息等。

为客户带来的意义：24小时营业时间，客户可以随时购物；不受地理位置影响，无论客户距离商店有多远，都可以轻松在线查找和购买商品。

#### 软件架构

项目采取前后端分离；后端又分为用户端和管理员端

用户端

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-0.1.png)

管理员端

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-0.2.png)

 

项目代码及页面的目录架构

工程目录结构

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-0.3.png)

 

用户端页面

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-0.4.png)

 

管理员端页面

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-0.5.png)

#### 功能模块：

用户功能：

浏览商品、加入购物车、下单、查看和修改自己的信息、个人订单管理-确认收货、给商家留言。

商城管理员功能：

用户管理：查看用户、添加用户、修改用户、删除用户；

商品管理：

（1）管理商品：查看商品、添加商品、修改商品、删除商品、将商品设置为热卖、将商品设置为特价；

（2）管理商品大类：查看商品大类、添加商品大类、修改商品大类、删除商品大类；

（3）管理商品小类：查看商品小类、添加商品小类、修改商品小类、删除商品小类；

订单管理：查看订单详情、审核通过、卖家已发货；

留言管理：查看留言、回复留言、删除留言；

公告管理：查看公告、添加公告、修改公告、删除公告；

新闻管理：查看新闻、添加新闻、修改新闻、删除新闻；

标签管理：查看标签、添加标签、修改标签、删除标签；

系统管理：修改密码、安全退出、刷新系统缓存。

#### 关键功能介绍

##### 4.0 项目特色功能

###### 4.0.1 邮箱功能

**邮箱功能：**用户在注册，登录时可选择邮箱验证方式，商品成功购买后，同样有邮件提示。

**业务流程：**根据用户提供的邮箱地址，并判断当前触发的事件，根据不同事件设置对应的邮件文本，调用sendMail()方法，sendMail()调用JavaMailUtil()进行邮件收发件人地址等相关参数配置，并在smtp协议下将文本以ssl方式通过465端口发送。

**![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-1.1.png)**

 

###### 4.0.2导入及批量导入功能

**批量导入功能：**管理员可通过上传excel文件的形式批量导入excel文件内全部商品信息。

**业务流程：**管理员通过点击“批量导入”按钮，触发系统调用UploadHandleServlet()方法将excel文件首先上传到项目/WEB-INF/upload目录下，之后该方法判断此次文件类型，并据此调用readExcelServlet()方法进行excel文件的读取，readExcelServlet()在执行过程中，循环调用PoiUtil类的getCell ()方法逐行对excel文件每一个单元格的数据进行读取，并储存在Product类的实例对象中，每读取完一行数据，就将该实例对象返回，并进一步调用initializeServlet()方法将该行商品数据存入数据库。



![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-1.2.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-1.3.png)

###### 4.0.3 商品信息批量导出功能

**批量导出：**管理员可对全部商品的详细信息进行导出，并存储在excel文件中

**业务流程：**管理员通过点击“导出商品信息”按钮，触发系统调用writeToExcel()方法，该方法调用数据库相关方法与数据库交互，将全部商品信息存储到List<Product>型变量productList中，之后生成excel文件存储路径/WEB-INF/summarize，并进一步调用ExcelWriter()功能，对productList中的每一个元素逐行写入到excel文件中，并将该文件以summary[(num)].xlsx的形式储存到提供的路径中，最终返回文件展示页面。

**![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-1.4.png)**

###### 4.0.4 导出文件压缩功能

**文件压缩：**管理员在选择将商品信息导出后，可选择将多次导出的excel文件批量压缩为rar格式压缩包，以节约系统空间并方便管理与取用。

**业务流程：**在管理员选择将商品信息导出后，可通过点击“压缩导出信息”按钮，触发系统zip()方法，该方法将自动检索储存导出信息的文件目录，并在指定压缩文件储存地址及储存格式rar，并调用ZipUtil方法将该目录内所有文件打包压缩，并返回文件展示页面。

**运行截图：**

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-1.5.png)

###### 4.0.5 文件下载功能

**文件下载：**每次生成导出文件后，系统自动检索导出文件目录，并将导出文件目录下的所有文件以链接的形式展示到文件展示页面。

**业务流程****:**每次生成导出文件后，系统自动调用ListFile()方法，该方法将检索储存导出文件的路径，并将该路径内的每一个文件以File格式储存，并传递到listFile页面中，在该页面中会为每一个文件生成一个downurl格式的链接，通过点击这些链接，可触发DownLoad()方法，该方法负责将文件以流的形式下载到本地机器中。

 

###### 4.0.6 加密功能

**加密功能：**为保证用户隐私安全，在向数据库存储用户信息前，会对用户敏感信息进行加密，加密原理为哈希散列。

**业务流程：**用户在点击“注册”按钮后，触发系统UserAction.register()方法，在该方法内会对将封装好的用户信息user类实例对象中包含的密码，电话，收货地址等敏感信息进行提取，并调用DigestUtil. sha1()方法对这些信息进行哈希散列，并将散列处理后的信息重新封装到实例对象中，最终调用userService.save()方法将用户信息系储存到数据库中。



##### 4.1 用户相关核心功能

###### 4.1.1加入购物车功能

**加入购物车**：用户将商品加入购物车

**业务流程**：根据访问页面地址栏的shopping_*.action调用相应的ShoppingAction类中addShoppingCartItem()方法，addShoppingCartItem()方法调用业务逻辑实现类ProductServiceImpl中的getProductById(int productId)方法，getProductById(int productId)方法调用BaseDAOImpl实现类中的get(Class<T> c,Serializable id)方法，最后根据addShoppingCartItem()方法中的retrun返回相应的页面。

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.1.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.2.png)

 

###### 4.1.2商品购买功能

**下单：**用户购买某件商品

**业务流程**：根据访问页面地址栏的shopping_*.action调用相应的ShoppingAction类中buy()方法，buy()方法调用业务逻辑实现类ProductServiceImpl中的getProductById(int productId)方法，getProductById(int productId)方法调用BaseDAOImpl实现类中的get(Class<T> c,Serializable id)方法，最后根据buy()方法中的retrun返回相应的页面。

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.3.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.4.png)

###### 4.1.3 确认收货功能

**个人订单管理****-****确认收货**：收到货物后，确认收货

**业务流程：**根据访问页面地址栏的order_*.action调用相应的OrderAction类中confirmReceive()方法，confirmReceive()方法调用业务逻辑实现类OrderServiceImpl中的updateOrderStatus(int status,String orderNo)方法，updateOrderStatus(int status,String orderNo)方法调用BaseDAOImpl实现类中的excuteHql(String hql,List<Object> param)方法，最后根据confirmReceive()方法中的retrun返回相应的页面。

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.5.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.6.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.7.png)

###### 4.1.4 留言功能

**给商家留言：**与商家沟通，询问商品的信息

**业务流程：**根据访问页面地址栏的comment_*.action调用相应的CommentAction类中save()方法，save()方法调用业务逻辑实现类CommentServiceImpl中的saveComment(Comment comment)方法，saveComment(Comment comment)方法调用BaseDAOImpl实现类中的merge(T o)方法，最后根据save()方法中的retrun返回相应的页面。



![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.8.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-2.9.png)

 

##### 4.2 管理员相关核心功能

###### 4.2.1 商品的增删改查功能

**增删改查：**管理员可通过后台管理系统对商品进行增删改查的基本操作。

**业务流程：**管理员进入后台管理系统后，按需要点击“添加”，“删除”，“修改”及“查询”按钮，可对应触发系统的增删改查方法，完成商品信息的即时管理。

###### 4.2.2热卖功能

**商品管理：**将商品设置为热卖

**业务流程：**先调用productManage.jsp页面的function函数的setProductHot()方法，  setProductHot()方法根据访问的product_setProductWithHot.action调用相应的ProductAction类中setProductWithHot()方法，setProductWithHot()方法调用业务逻辑实现类ProductServiceImpl中的setProductWithHot(int productId)方法，setProductWithHot(int productId)方法调用BaseDAOImpl实现类中的get(Class<T> c,Serializable id)方法和save(T o)方法，最后根据productManage.jsp页面的function函数的setProductHot()方法设置成功。



![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.2.1.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.2.2.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.2.3.png)

###### 4.2.3 特价功能

**商品管理**：将商品设置为特价

**业务流程：**先调用productManage.jsp页面的function函数的setProductSpecialPrice()方法，  setProductSpecialPrice()方法根据访问的product_setProductSpecialPrice.action调用相应的ProductAction类中setProductSpecialPrice()方法，setProductSpecialPrice()方法调用业务逻辑实现类ProductServiceImpl中的setProductSpecialPrice(int productId)方法，setProductSpecialPrice(int productId)方法调用BaseDAOImpl实现类中的get(Class<T> c,Serializable id)方法和save(T o)方法，最后根据productManage.jsp页面的function函数的setProductSpecialPrice()方法设置成功。



![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.3.1.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.3.2.png)

###### 4.2.4 审核功能

**订单管理：**审核通过

**业务流程：**先调用orderManage.jsp页面的function函数的modifyOrderStatus(status)方法，  modifyOrderStatus(status)方法根据访问的order_modifyOrderStatus.action调用相应的OrderAction类中modifyOrderStatus()方法，modifyOrderStatus()方法调用业务逻辑实现类OrderServiceImpl中的updateOrderStatus(int status,String orderNo)方法，updateOrderStatus(int status,String orderNo)方法调用BaseDAOImpl实现类中的excuteHql(String hql,List<Object> param)方法，最后根据OrderManage.jsp页面的function函数的modifyOrderStatus(status)方法审核通过。

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.4.1.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.4.2.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.4.3.png)

###### 4.2.5 确认发货功能

**订单管理：**卖家已发货

**业务流程：**先调用orderManage.jsp页面的function函数的modifyOrderStatus(status)方法，  modifyOrderStatus(status)方法根据访问的order_modifyOrderStatus.action调用相应的OrderAction类中modifyOrderStatus()方法，modifyOrderStatus()方法调用业务逻辑实现类OrderServiceImpl中的updateOrderStatus(int status,String orderNo)方法，updateOrderStatus(int status,String orderNo)方法调用BaseDAOImpl实现类中的excuteHql(String hql,List<Object> param)方法，最后根据OrderManage.jsp页面的function函数的modifyOrderStatus(status)方法修改成功。

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.5.1.png)

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-4.2.5.2.png)

#### 参与贡献

1.  系统环境：Windows10
2.  开发工具：Eclipse
3.  Java版本：JDK 1.8
4.  服务器：tomcat 8.5
5.  数据库：MySQL 5.7
6.  系统采用技术：Spring+Struts2+Hibernate
7.  辅助工具：Gitee

#### 项目的主要特色介绍

###### 功能方面

1. 完全实现了前端和后台分离，前端面向用户，后端面向管理员，极大程度上降低了程序功能的耦合度。
2. 加入了邮箱系统，实现了邮箱验证码，邮箱提示商品发货信息等即时通知，且该邮箱系统采用465端口进行ssl连接，而非25端口的普通连接，能够胜任在远程云服务器的正常使用，进一步增强真实性及实用性。
3. 加入了对文件的的读入和存取操作（导入导出），间接实现了数据库文件与excel文件的交互，降低了数据信息导入导出的难度，贴合了实际应用场景。
4. 加入了文件压缩功能，可将多个目录下的多个文件打包压缩到一个rar文件中，方便了导出文件的管理，减少了对系统资源的占用，进一步增强程序实用性和真实性。
5. 加入了加密技术，不再简单地将用户输入的信息直接存入数据库，而是将隐私信息进行哈希散列后再存入数据库，增强了数据信息的安全性，增强程序真实性和实用性。
6. 部分页面的跳转加入了页面跳转倒计时，缓解程序在与数据库交互过程中带来的延迟带来的不流畅感，增加程序真实性。

###### 技术方面

1. 项目全程利用Gitee仓库辅助开发，做到了多人同步协同开发，解决了开发工具版本不统一带来的整合问题。
2. 采用了SSH框架，SSH框架的系统从职责上分为四层：表示层、业务逻辑层、数据持久层和域模块层。

![img](https://treathy.com/wp-content/uploads/2023/12/javaweb5-5.1.jpg)

这种Struts+Hibernate+Spring的三大框架整合，契合着MVC模式的三层对象。其中Struts对应着前台的控制层，而Spring则负责实体bean的业务逻辑处理，至于Hibernate则是负责数据库的交接以及使用Dao接口来完成操作。

**技术优势：**SSH组合框架技术优势体现在四个方面：

1. 分离了Java代码和HIML代码，降低了对开发人员要求的复合度。
2. 是系统的层与层之间的工作相是对独立的，代码耦合度低。
3. 即使脱离了Spring 环境的AOP机制，也不会妨碍AOP实现业务的功能。
4. 与SSH配合使用的Hibemate等跨平台技术开源性极强促使了SSH框架的飞速发展。 SSH的技术优势使得采用SSH框架技术开发的系统具备了很强的可拓展性、可移植性。同时，采用开源的SSH框架能够大大简化系统开发的复杂度，缩短系统开发时间。

##### 关键数据库表的定义：

######  t_user表（用户信息表）

内含字段

（1）  id：用户编号

（2）  username：用户名

（3）  password：密码（隐私信息密文储存）

（4）  email：邮箱地址

（5）  status：用户身份（1 普通用户2 管理员）

（6）  birthday：出生日期

（7）  dentityCode：身份证号（隐私信息密文储存）

（8）  mobile：联系电话（隐私信息密文储存）

（9）  sex：性别

（10）trueName：用户真实姓名

（11）address：收货地址（隐私信息密文储存）

######  t_product表（商品信息表）

（1） id：商品编号

（2） name：商品名称

（3） description：商品描述

（4） price：价格

（5） stock：库存

（6） proPic：商品图片在项目下的存储路径

（7） smallTypeId：商品所属小类

（8） bigTypeId：商品所属大类

（9） hot：当前是否热卖

（10）      hotTime：热卖时间

（11）      specialPrice：当前是否特价

（12）      specialPriceTime：特价时间

###### t_order表（订单信息表）

（1） id：订单编号

（2） cost：该笔订单总金额

（3） createTime：订单创建时间

（4） orderNo：订单号（不同订单编号可能对应同一订单号，即一次支付多笔订单对应同一订单号）

（5） status：当前状态 （1 待审核 2 审核通过 3 卖家已发货 4 已收获）

（6） userId：订单对应用户编号

###### t_order_product表（订单-商品关联表）

（1） id：订单-商品关联信息编号

（2） num：该笔订单对应商品数量

（3） orderId：订单编号

（4） productId：商品编号

######  t_smalltype表

（1） id：商品小类编号

（2） name：名称

（3） bigTypeId：所属大类编号

（4） remarks：备注
