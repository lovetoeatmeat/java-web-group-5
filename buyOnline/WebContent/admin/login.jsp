﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>登录</title>
	
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/base.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/loginStyle.css">

</head>
<body>

    <div class="bg"></div>
    <div class="container">
        <div class="line bouncein">
            <div class="xs6 xm4 xs3-move xm4-move">
                <div style="height:150px;"></div>
                <div class="media media-y margin-big-bottom">
                </div>
                <form id="loginForm" method="post" action="${pageContext.request.contextPath}/user_login.action" onsubmit="return checkForm()">
                    <div class="panel loginbox">
                        <div class="text-center margin-big padding-big-top">
                            <h1>在线购物平台管理中心</h1>
                        </div>
                        <div class="panel-body" style="padding:30px; padding-bottom:10px; padding-top:10px;">
                            <div class="form-group">
                                <div class="field field-icon-right">
                                    <input type="text" class="input input-big" name="user.userName" id="username" placeholder="登录账号" />
                                    <span class="icon icon-user margin-small"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="field field-icon-right">
                                    <input type="password" class="input input-big" name="user.password" id="password"  placeholder="登录密码" />
                                    <span class="icon icon-key margin-small"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="field">
                                    <input type="text" class="input input-big" id="imageCode" name="imageCode" placeholder="填写右侧的验证码" />
                                    <img onclick="javascript:loadimage();" title="换一张试试" name="randImage"
									id="randImage" src="${pageContext.request.contextPath}/admin/image.jsp" width="100" height="32" border="1"
									align="absmiddle" class="passcode" style="height:43px;cursor:pointer;">
                                </div>
                            </div>
                        </div>
                        <div style="padding:30px;">
                        	<input type="hidden" name="user.status" value="2"/>
                            <input type="submit" name="button" class="button button-block bg-main text-big input-big" value="登录">
                            <font id="error"  color="red">${error }</font>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">
	function loadimage(){
		document.getElementById("randImage").src = "image.jsp?"+Math.random();
	}

	function checkForm(){
		 var userName=$("#userName").val();
		 var password=$("#password").val();
		 if(userName==""){
			 $("#error").html("用户名不能为空！");
			 return false;
		 }
		 if(password==""){
			 $("#error").html("密码不能为空！");
			 return false;
		 }
		 if(imageCode==""){
			 $("#error").html("验证码不能为空！");
			 return false;
		 }
		 return true;
	}
</script>
</body>
</html>