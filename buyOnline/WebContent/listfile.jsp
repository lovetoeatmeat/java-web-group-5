<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<title>下载文件显示页面</title>
</head>

<script type="text/javascript">
function writeToExcel()
{
	listFileForm.action="writeToExcelServlet";
	listFileForm.submit();
}

function zip()
{
	listFileForm.action="zipServlet";
	listFileForm.submit();
}
</script>
<body>
	<form name=listFileForm action="writeToExcelServlet" method="post">
		<table align="center" border="1" cellspacing="0" cellpadding="0">
			<tr>
				<td><font align="center" color=red> 批量导出商品信息 </font></td>
				<td><input type="button" onclick="writeToExcel()" name="leadingOut" value="开始导出"></td>
			</tr>
			<tr>
				<td><font align="center" color=red> 压缩导出的商品文件 </font></td>
				<td><input type="button" onclick="zip()" name="zipButton" value="压缩导出"></td>
			</tr>
			
		</table>
	</form>
	<!-- 遍历Map集合 -->
	<c:forEach var="me" items="${fileNameMap}">
		<c:url value="DownLoadServlet" var="downurl">
			<c:param name="filename" value="${me.key}"></c:param>
		</c:url>
        ${me.value}<a href="${downurl}">下载</a>
		<br />
	</c:forEach>
</body>
</html>
