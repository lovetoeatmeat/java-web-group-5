package com.buyOnline.DbcpPoolPackage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class needToClose {
	private Connection conn;
	private Statement statement;
	private ResultSet rs;
	
	public Connection getConn() {
		return conn;
	}
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	public Statement getStatement() {
		return statement;
	}
	public void setStatement(Statement statement) {
		this.statement = statement;
	}
	public ResultSet getRs() {
		return rs;
	}
	public void setRs(ResultSet rs) {
		this.rs = rs;
	}
	
	public void setAll(Connection conn,Statement statement,ResultSet rs)
	{
		this.conn = conn;
		this.statement = statement;
		this.rs = rs;
	}

}
