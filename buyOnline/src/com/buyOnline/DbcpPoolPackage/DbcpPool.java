package com.buyOnline.DbcpPoolPackage;

import java.sql.*;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.BasicDataSourceFactory;

public class DbcpPool {
	
	private static BasicDataSource dataSource=null;
	
	public static void init()
	{
		if(dataSource!=null)
		{
			try
			{
				dataSource.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			dataSource=null;
		}
		try
		{
			Properties p=new Properties();
			p.setProperty("driverClassName", "com.mysql.jdbc.Driver");
			p.setProperty("url", "jdbc:mysql://localhost:3306/db_ebuy2?useUnicode=true&characterEncoding=UTF-8");
			p.setProperty("username", "root");
			p.setProperty("password", "root");
			p.setProperty("maxActive", "30");
			p.setProperty("maxIdle", "10");
			p.setProperty("maxWait", "1000");
			p.setProperty("removeAbandoned", "false");
			p.setProperty("removeAbandonedTimeout", "120");
			p.setProperty("testOnBorrow", "true");
			p.setProperty("logAbandoned", "true");
			dataSource=(BasicDataSource)BasicDataSourceFactory.createDataSource(p);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static synchronized Connection getConnection()throws SQLException
	{
		if(dataSource==null)
		{
			init();
		}
		Connection conn=null;
		if(dataSource!=null)
		{
			conn=dataSource.getConnection();
		}
		return conn;
	}
	
	public static void closeAll(needToClose newNeedtoClose) {
		// TODO Auto-generated method stub
		Connection conn=newNeedtoClose.getConn();
		Statement statement=newNeedtoClose.getStatement();
		ResultSet rs=newNeedtoClose.getRs();
		
		if(rs!=null)
		{
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(statement!=null)
		{
			try {
				statement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(conn!=null)
		{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static needToClose executeQuery(Connection conn,String sql) {
		// TODO Auto-generated method stub
		needToClose newNeedtoClose=new needToClose();
		try {
			Statement statement=conn.createStatement();
			ResultSet rs=statement.executeQuery(sql);
			newNeedtoClose.setAll(conn, statement, rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newNeedtoClose;
	}

	public static needToClose executePreparedStatement(Connection conn, String sql,String argument) {
		// TODO Auto-generated method stub
		needToClose newNeedtoClose=new needToClose();
		try {
			PreparedStatement psQuery=conn.prepareStatement(sql);
			psQuery.setString(1, argument);
			ResultSet rs=psQuery.executeQuery();
			newNeedtoClose.setAll(conn, psQuery, rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newNeedtoClose;
	}
	
	public static needToClose executePreparedStatement(Connection conn, String sql, String argumentA, String argumentB) {
		// TODO Auto-generated method stub
		needToClose newNeedtoClose=new needToClose();
		try {
			PreparedStatement psQuery=conn.prepareStatement(sql);
			psQuery.setString(1, argumentA);
			psQuery.setString(2, argumentB);
			ResultSet rs=psQuery.executeQuery();
			newNeedtoClose.setAll(conn, psQuery, rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newNeedtoClose;
	}

	public static PreparedStatement executePreparedStatementReturnPS(Connection conn, String sql) {
		// TODO Auto-generated method stub
		PreparedStatement psQuery=null;
		try {
			psQuery=conn.prepareStatement(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return psQuery;
	}



	
	

}
