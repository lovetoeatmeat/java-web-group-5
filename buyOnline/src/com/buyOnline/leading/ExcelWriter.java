package com.buyOnline.leading;



import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.buyOnline.entity.Product;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ExcelWriter {

    private static List<String> CELL_HEADS;

    static{
        CELL_HEADS = new ArrayList<String>();
        CELL_HEADS.add("����");
        CELL_HEADS.add("����");
        CELL_HEADS.add("�۸�");
        CELL_HEADS.add("���");
    }

    public static Workbook exportData(List<Product> productList){
        Workbook workbook = new SXSSFWorkbook();

        //Workbook workbook = new HSSFWorkbook();

        Sheet sheet = buildDataSheet(workbook);
        int rowNum = 1;
        for (Iterator<Product> it = productList.iterator(); it.hasNext(); ) {
        	Product data = it.next();
            if (data == null) {
                continue;
            }
            Row row = sheet.createRow(rowNum++);
            convertDataToRow(data, row);
        }
        return workbook;
    }

    private static Sheet buildDataSheet(Workbook workbook) {
        Sheet sheet = workbook.createSheet();
        for (int i=0; i<CELL_HEADS.size(); i++) {
            sheet.setColumnWidth(i, 4000);
        }
        sheet.setDefaultRowHeight((short) 400);
        CellStyle cellStyle = buildHeadCellStyle(sheet.getWorkbook());
        Row head = sheet.createRow(0);
        for (int i = 0; i < CELL_HEADS.size(); i++) {
            Cell cell = head.createCell(i);
            cell.setCellValue(CELL_HEADS.get(i));
            cell.setCellStyle(cellStyle);
        }
        return sheet;
    }

    private static CellStyle buildHeadCellStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        //style.setAlignment(HorizontalAlignment.CENTER);
        //style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        //style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        //style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        //style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        //style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        return style;
    }

    private static void convertDataToRow(Product productOption, Row row){
        int cellNum = 0;
        Cell cell;
        // name
        cell = row.createCell(cellNum++);
        cell.setCellValue(productOption.getName());
        // description
        cell = row.createCell(cellNum++);
        cell.setCellValue(productOption.getDescription());
        // price
        cell = row.createCell(cellNum++);
        cell.setCellValue(productOption.getPrice());
        // stock
        cell = row.createCell(cellNum++);
        cell.setCellValue(productOption.getStock());
    }
}
