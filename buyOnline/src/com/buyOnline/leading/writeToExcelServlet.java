package com.buyOnline.leading;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;

import com.buyOnline.dao.IProductDao;
import com.buyOnline.dao.impl.productDaoImpl;
import com.buyOnline.entity.Product;

/**
 * Servlet implementation class writeToExcelServlet
 */
@WebServlet("/writeToExcelServlet")
public class writeToExcelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(writeToExcelServlet.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public writeToExcelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		IProductDao productDaoInstance=new productDaoImpl();
		List<Product> productList=productDaoInstance.findAll();

        Workbook workbook = ExcelWriter.exportData(productList);

        FileOutputStream fileOut = null;
        try {
            String exportFilePath = this.getServletContext().getRealPath("/WEB-INF/summarize")+"\\summary.xlsx";
            System.out.println(exportFilePath);
            File folder = new File(this.getServletContext().getRealPath("/WEB-INF/summarize"));
    		if (!folder.exists()) {
    			folder.mkdir();
    		}
            File exportFile = new File(exportFilePath);
            if (!exportFile.exists()) {
                exportFile.createNewFile();
            }

            fileOut = new FileOutputStream(exportFilePath);
            workbook.write(fileOut);
            fileOut.flush();
        } catch (Exception e) {
            logger.warning("输出Excel时发生错误，错误原因：" + e.getMessage());
        } finally {
            try {
                if (null != fileOut) {
                    fileOut.close();
                }
                if (null != workbook) {
                    workbook.close();
                }
            } catch (IOException e) {
                logger.warning("关闭输出流时发生错误，错误原因：" + e.getMessage());
            }
        }
        request.getRequestDispatcher("ListFileServlet").forward(request, response);
        
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
