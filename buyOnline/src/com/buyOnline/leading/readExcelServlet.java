package com.buyOnline.leading;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.buyOnline.dao.IProductDao;
import com.buyOnline.dao.impl.productDaoImpl;
import com.buyOnline.entity.Product;



/**
 * Servlet implementation class readExcelServlet
 */
@WebServlet("/readExcelServlet")
public class readExcelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public readExcelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<Product> initialList=new ArrayList<Product>();
		String realFullFilePath=String.valueOf(request.getAttribute("realFullFilePath"));
		realFullFilePath="C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\webapps\\buyOnline\\WEB-INF\\upload\\leading in test.xlsx";
		PoiUtil poi = new PoiUtil();
		poi.setFileName(realFullFilePath);
		for(int row=1;row<7;row++)
		{
			Product productOption=new Product();
			for(int col=0;col<5;col++)
			{
				System.out.println("row:"+row+"\ncol:"+col);
				String cellValue=poi.getCell(0, row, col);
				switch(col)
				{
				case(0):productOption.setName(cellValue);
					break;
				case(1):productOption.setDescription(cellValue);
					break;
				case(2):productOption.setPrice(Integer.parseInt(cellValue));
					break;
				case(3):productOption.setStock(Integer.parseInt(cellValue));
					break;
				case(4):productOption.setProPic(cellValue);
					break;
				default:
					break;
				}
				System.out.println(poi.getCell(0, row, col));
			}
			initialList.add(productOption);
		}
		
		System.out.println("read"+initialList.get(0).getName());
		
		
		for(int i=0,result=0;i<initialList.size();i++)
		{
			Product initialOption=initialList.get(i);
			
			String name=initialOption.getName();
			System.out.println("aaa"+name);
			String description=initialOption.getDescription();
			int price=initialOption.getPrice();
			int stock=initialOption.getStock();
			String proPic=initialOption.getProPic();
			
			IProductDao productDaoInstance=new productDaoImpl();
			result=productDaoInstance.initialize(name, description, price, stock, proPic);
			if(result!=1)
			{
				break;
			}
			System.out.println(result);
		}
		request.getRequestDispatcher("admin/productManage.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
