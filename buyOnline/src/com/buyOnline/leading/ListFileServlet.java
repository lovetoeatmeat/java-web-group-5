package com.buyOnline.leading;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName: ListFileServlet
 * @Description:ListFile
 *  */
@WebServlet("/ListFileServlet")
public class ListFileServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String excelFilePath = this.getServletContext().getRealPath("/WEB-INF/summarize");
		Map<String, String> fileNameMap = new HashMap<String, String>();
		listfile(new File(excelFilePath), fileNameMap);
		request.setAttribute("fileNameMap", fileNameMap);
		request.getRequestDispatcher("/listfile.jsp").forward(request, response);
	}

	public void listfile(File file, Map<String, String> map) {
		if (!file.isFile()) {
			File files[] = file.listFiles();
			for (File f : files) {
				listfile(f, map);
			}
		} else {
			String realName = file.getName();
			map.put(file.getName(), realName);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
