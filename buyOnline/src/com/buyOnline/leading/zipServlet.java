package com.buyOnline.leading;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class zipServlet
 */
@WebServlet("/zipServlet")
public class zipServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public zipServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String sourceDir=this.getServletContext().getRealPath("/WEB-INF/summarize");
		String imagesDir=this.getServletContext().getRealPath("/images/product");
		String zipFilePath=this.getServletContext().getRealPath("/WEB-INF/summarize")+"\\summary.zip";
		String zipImagesPath=this.getServletContext().getRealPath("/WEB-INF/summarize")+"\\productImages.zip";
		// ZipUtil
		try 
		{
			ZipUtil.doZip(sourceDir, zipFilePath);
			ZipUtil.doZip(imagesDir, zipImagesPath);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		request.getRequestDispatcher("ListFileServlet").forward(request, response);
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
