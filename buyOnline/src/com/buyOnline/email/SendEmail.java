package com.buyOnline.email;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

	 
	 
	public class SendEmail {
			public static String emailAccount = "@qq.com";
			public static String emailPassword = "";
			public static String emailSMTPHost = "smtp.qq.com";
			public static String receiveMailAccount = "";
			
			public static  MimeMessage creatMimeMessage(Session session,String sendMail,String receiveMail,String html) throws MessagingException, IOException 
			{
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(sendMail, "在线购物平台", "UTF-8"));
				message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "张三", "UTF-8"));
				message.setSubject("卖家已发货","UTF-8");
				message.setContent(html,"text/html;charset=UTF-8");
				message.setSentDate(new Date());
				message.saveChanges();
				/*OutputStream out = new FileOutputStream("C://MyEmail" + UUID.randomUUID().toString() + ".eml");
				message.writeTo(out);
				out.flush();
				out.close();*/
				return message;
			} 		
			
			
		public static void sendMail(String email,String orderNO) throws  IOException {
			try {
				SendEmail.receiveMailAccount = email; 
	 				Properties props = new Properties();
				props.setProperty("mail.debug", "true");
				props.setProperty("mail.smtp.auth", "true");
				int port=465;
				props.setProperty("mail.smtp.port",Integer.toString(port));
				props.setProperty("mail.smtp.ssl.enable", "true"); 
				props.setProperty("mail.smtp.socketFactory.port", Integer.toString(port));
				props.setProperty("mail.smtp.socketFactory.fallback", "false");
				props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				props.setProperty("mail.host", SendEmail.emailSMTPHost);
				props.setProperty("mail.transport.protocol", "smtp");
	 
				Session session = Session.getInstance(props);
				session.setDebug(true);
				String text="您的订单"+orderNO+"卖家已发货\n请您及时关注物流信息";
				MimeMessage message = SendEmail.creatMimeMessage(session, SendEmail.emailAccount,
						SendEmail.receiveMailAccount, text);
	 
				Transport transport = session.getTransport();
				transport.connect(SendEmail.emailAccount, SendEmail.emailPassword);
				transport.sendMessage(message, message.getAllRecipients());
				transport.close();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("邮件发送失败");
			}
		}
		

}
