package com.buyOnline.dao;

import java.util.List;

import com.buyOnline.entity.Product;


public interface IProductDao {	
	public List<Product> findAll();
	
	public int initialize(String name,String description,int price,int stock, String proPic);

}
