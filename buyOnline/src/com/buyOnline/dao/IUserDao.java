package com.buyOnline.dao;

import com.buyOnline.entity.User;

public interface IUserDao {
	
	public User findByID(int id);

}
