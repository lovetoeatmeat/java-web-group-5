package com.buyOnline.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.buyOnline.DbcpPoolPackage.DbcpPool;
import com.buyOnline.DbcpPoolPackage.needToClose;
import com.buyOnline.dao.IUserDao;
import com.buyOnline.entity.Order;
import com.buyOnline.entity.User;

public class userDaoImpl implements IUserDao {

	public User findByID(int id) {
		// TODO Auto-generated method stub
		String sql="SELECT * FROM t_user WHERE id=?";
		User userOption=new User();
		Connection conn = null;
		try {
			conn = DbcpPool.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		needToClose newNeedtoClose=DbcpPool.executePreparedStatement(conn,sql,String.valueOf(id));
		ResultSet rs=newNeedtoClose.getRs();
		
		try
		{
			if(rs.next())
			{
				userOption.setId(rs.getInt("id"));
				userOption.setEmail(rs.getString("email"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		DbcpPool.closeAll(newNeedtoClose);
		return userOption;
	}

}
