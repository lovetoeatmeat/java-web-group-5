package com.buyOnline.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.buyOnline.dao.IOrderDao;
import com.buyOnline.dao.IUserDao;
import com.buyOnline.entity.Order;

import com.buyOnline.DbcpPoolPackage.*;

public class orderDaoImpl implements IOrderDao {

	public Order findByOrderNO(String orderNo) {
		// TODO Auto-generated method stub
		String sql="SELECT * FROM t_order WHERE orderNo=?";
		Order orderOption=new Order();
		Connection conn = null;
		try {
			conn = DbcpPool.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		needToClose newNeedtoClose=DbcpPool.executePreparedStatement(conn,sql,orderNo);
		ResultSet rs=newNeedtoClose.getRs();
		
		try
		{
			System.out.println("In try");
			if(rs.next())
			{
				System.out.println("In if");
				orderOption.setId(rs.getInt("id"));
				orderOption.setCost(rs.getFloat("cost"));
				//orderOption.setCreateTime(Timestamp.valueOf(rs.getString("createTime")));
				orderOption.setOrderNo(rs.getString("orderNo"));
				orderOption.setStatus(rs.getInt("status"));
				IUserDao userDaoInstance=new userDaoImpl();
				orderOption.setUser(userDaoInstance.findByID(rs.getInt("userId")));
				System.out.println(orderOption.getUser().getId()+"aaa");
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
		DbcpPool.closeAll(newNeedtoClose);
		return orderOption;
	}

}
