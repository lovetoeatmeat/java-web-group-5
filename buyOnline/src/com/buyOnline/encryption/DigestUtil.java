package com.buyOnline.encryption;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DigestUtil {
	private static String toHexString(byte[] byteArray) {
		if (byteArray == null || byteArray.length < 1)
			throw new IllegalArgumentException("this byteArray must not be null or empty");
 
		final StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < byteArray.length; i++) {
			if ((byteArray[i] & 0xff) < 0x10)
				hexString.append("0");
			hexString.append(Integer.toHexString(0xFF & byteArray[i]));
		}
		return hexString.toString().toLowerCase();
	}
	
	
	public static String md5(String s) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("md5");
			return toHexString(md5.digest(s.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	
	public static String sha1(String s) {
		try {
			MessageDigest sha1 = MessageDigest.getInstance("sha1");
			return toHexString(sha1.digest(s.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}
}
